# 汇编语言实验

## 1. 概述
本实验将展示如何使用PlatformIO创建一个可在RVfpga上运行的RISC-V汇编语言程序。

RISC-V汇编语言包含用于实现高级代码的简单指令。例如add、sub和mul等常用的RISC-V指令（分别用于对两个操作数进行加法、减法和乘法运算）。

RISC-V指令的基本类型包括：计算（算术、逻辑和移位）指令、存储器操作和分支/跳转。

下表中列出了最常用的RISC-V指令。

![常用的RISC-V汇编指令](image_2022010701.png)

指令使用的操作数位于寄存器或存储器中，或者被编码为常量（即立即数）。

RISC-V包含32个32位寄存器，下表列出了这32个RISC-V寄存器的名称。

![RISC-V寄存器](image_2022010702.png)

这些寄存器可以使用寄存器名称（例如zero、s0、t5等）或寄存器编号（即x0、x8、x30）来指定。编程器通常使用寄存器名称，其中保留有一些关于寄存器典型用途的信息。例如，保存寄存器s0-s11通常用于程序变量，而临时寄存器t0-t6用于临时计算。zero寄存器（x0）中始终包含值0，因为程序中通常需要该值。其他寄存器也均有特定用途，如上表所示。

除了实际的RISC-V指令，RISC-V还包括伪指令，伪指令并非真正的RISC-V指令，但编程器经常使用。伪指令是使用一个或多个真正的RISC-V指令实现的。例如，移动伪指令（mv s1, s2）会复制s2的内容并将其放入s1。该指令是使用以下真正的RISC-V指令实现的：addi s1, s2, 0。

以句点开头的命令为汇编器指令。这些指令是汇编器需执行的命令，而不是汇编器要转换的代码。这些指令会告知汇编器放置代码和数据的位置，指定在程序中使用的文本和数据常量等。下表列出了RISC-V的主要汇编器指令（《RISC-V读取器：开源架构Atlas》，作者：Patterson & Waterman，© 2017）。

![RISC-V主要指令](image_2022010703.png)

有关RISC-V汇编语言的更多详细信息，请参见[《RISC-V指令集手册》](https://github.com/riscv/riscv-isa-manual/releases/download/Ratified-IMAFDQC/riscv-spec-20191213.pdf)

## 2. RVfpga编写RISC-V汇编程序
在编写程序之前，请按照以下步骤设置PlatformIO项目，然后在RVfpga上创建并运行汇编程序：

- 创建RVfpga项目
- 编写RISC-V汇编语言程序
- 将RVfpga下载到Nexys4 DDR FPGA开发板
- 编译、下载和运行汇编程序

### 2.1 创建RVfpga项目
启动VSCode，如果VScode启动后PlatformIO没有自动打开，请单击左侧功能区菜单中的PlatformIO图标，然后单击“PIO Home”（PIO主页）→“Open”（打开）（如下图所示）。

![打开PlatformIO并创建新项目](image_2022010704.png)

在“PIO Home”（PIO主页）欢迎窗口中，单击“New Project”（新建项目），如上图所示。

这里假设将项目命名为“Project1”，在“Board”（电路板）部分选择“RVfpga: Digilent Nexys A7”（输入RVfpga，即可出现该选项）。保留框架的默认选项WD-framework（Western Digital框架，其中包含Freedom-E SDK gcc和gdb）。取消选中“Use default location”（使用默认位置），然后将程序放到以下路径：[RVfpgaPath]/RVfpga/Labs/Lab3（如下图所示）。

![为项目命名并选择电路板和项目文件夹](image_2022010705.png)

然后点击窗口底部的“Finish”（完成）。

在左侧“Explorer”（资源管理器）窗格的PROJECT1下（可能需要展开），双击platformio.ini打开该文件（如下图所示）。该文件为PlatformIO初始化文件。

![PlatformIO初始化文件：platformio.ini](image_2022010706.png)

将要加载到FPGA上的RVfpga比特流文件路径添加到platformio.ini文件，如下图所示，按Ctrl-s保存platformio.ini文件。

![添加RVfpga比特流文件（rvfpga.bit）的位置](image_2022010707.png)

### 2.2 编写RISC-V汇编语言程序
单击“File”（文件）→“New File”（新建文件）（如下图所示）。

![向项目中添加文件](image_2022010708.png)

将打开一个空白窗口。在该窗口中输入RISC-V汇编程序。程序输入完成后，按Ctrl-s保存文件。

### 2.3 将RVfpga下载到Nexys4 DDR FPGA开发板
单击左侧功能区菜单中的PlatformIO图标 ![PlatformIO图标](image_2022010709.png)，展开“Project Tasks”（项目任务）→ env:swervolf_nexys →“Platform”（平台），然后单击“Upload Bitstream”（上传比特流），将RVfpga下载到Nexys4 DDR开发板。

### 2.4 编译、下载和运行RISC-V汇编程序
单击左侧功能区菜单中的“Run”（运行）按钮，然后单击“Start Debugging”（开始调试）按钮，如下图所示。

![在RVfpga上运行程序](image_2022010710.png)

程序将下载至Nexys4 DDR开发板的FPGA上所运行的RVfpga。此时即可开始运行和调试程序，如下图所示。

![在RVfpga上运行的程序](image_2022010711.png)

### 2.5 Whisper模拟器上调试RISC-V汇编程序

[Whisper](https://github.com/chipsalliance/SweRV-ISS)是一个RISC-V指令集模拟器(ISS)。它允许用户运行RISC-V代码，而不需要底层的RISC-V硬件。
因此，这里可以使用Whisper在PlatformIO测试、运行和调试汇编程序，而无需Nexys4 DDR FPGA开发板。

双击打开platformio.ini文件，添加Whisper模拟器作为调试工具，如下图所示。

![添加Whisper模拟器作为调试工具](image_2022082001.png)

单击左侧功能区菜单中的“Run”（运行）按钮，然后单击“Start Debugging”（开始调试）按钮，如下图所示。

![在RVfpga上运行程序](image_2022010710.png)

此时，可以像在2.4节中描述的那样调试程序。只不过这次程序是在Whisper上模拟运行，而不是Nexys4 DDR FPGA开发板上。

如果在Whisper上模拟运行的程序使用了printfNexys函数，千万不要连接串口终端（不管是PlatformIO的串口终端，还是单独的串口终端程序），因为此时串口信息是在打开的DEBUG控制台中显示。

#### 注：Whisper模拟器上运行的程序最好与硬件不相关，如果相关可能需要Verilator仿真环境及RVfpga模拟程序支持（具体方法见相应的文档说明）

## 3. 动手实验
### 3.1 编写一个RISC-V汇编程序，使开关的值在LED上闪烁。该值应以足够慢的速度进行脉冲开关，让人眼可以观察到值在闪烁。

### 3.2 编写一个RISC-V汇编程序，不断增加点亮并滚动的LED的数量，直到所有LED都点亮为止。然后该模式应重复进行。
该程序应产生以下效果：

- 1.首先，有一个点亮的LED从右向左滚动。
- 2.到达最左侧的LED后，应有两个点亮的LED从左向右滚动，然后又从右向左滚动。
- 3.到达最左侧的两个LED后，应有三个点亮的LED从左向右滚动，然后又从右向左滚动。
- 4.接下来应有四个点亮的LED滚动。
- 5.依此类推，直到所有LED均点亮。
- 6.然后该模式应重复进行。

### 3.3 编写一个RISC-V汇编程序，根据欧几里得算法求出a和b两个数的最大公约数。a和b两个值在程序中应该是静态定义的变量。
有关欧几里得算法的更多信息，[请访问](https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/the-euclidean-algorithm)。

### 3.4 用RISC-V汇编语言实现冒泡排序算法
算法通过以下方式按升序对向量分量进行排序：

- 1.反复遍历向量直至完成。
- 2.如果两个相邻分量V(i) > V(i+1)，则互换其位置。
- 3.当每对相邻分量的顺序均正确时，算法停止。

### 3.5 用RISC-V汇编语言编写一个程序，通过迭代乘法计算给定非负数n的阶乘。
虽然应使用多个n值测试程序，但最终提交的应为n = 7时的结果。n应为程序内静态定义的变量。
